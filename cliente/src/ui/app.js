const { ipcRenderer } = require('electron');
const fs = require('fs');
const http = require('http');


const visitorNameInput = document.getElementById('visitorNameInput');
const addVisitorButton = document.getElementById('addVisitorButton');
const getDocumentButton = document.getElementById('getDocumentButton');
const documentDataElement = document.getElementById('documentData'); 
const fileUpload = document.getElementById('fileUpload');
const uploadButton = document.getElementById('uploadButton'); 

ipcRenderer.invoke('getCounter')
  .then((data) => {
    document.getElementById('score').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });

const refreshCounters = () => {
ipcRenderer.invoke('getCounter')
    .then((data) => {
    document.getElementById('score').textContent = data;
    })
    .catch((err) => {
    console.log(err);
    });

    ipcRenderer.invoke('getMaxScore')
  .then((data) => {
    document.getElementById('maxScore').textContent = data;
  })
  .catch((err) => {
    console.log(err);
  });
};

const refreshVisitorList = () => {
  ipcRenderer.invoke('getVisitors')
    .then((data) => {
      const visitorNamesElement = document.getElementById('visitors');
      visitorNamesElement.innerHTML = '';
      data.forEach(visitor => {
        const p = document.createElement('p');
        p.innerText = `Name: ${visitor.name}, Role: ${visitor.role}`;
        visitorNamesElement.appendChild(p);
      });
    })
    .catch((err) => {
      console.log(err);
    });
};


document.addEventListener('DOMContentLoaded', () => {

const add1Button = document.getElementById('add1Button');
add1Button.addEventListener('click', () => {
  add1ToCounter();
});
const add1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const add10Button = document.getElementById('add10Button');
add10Button.addEventListener('click', () => {
  add10ToCounter();
});
const add10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/add10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract1Button = document.getElementById('Subtract1Button');
subtract1Button.addEventListener('click', () => {
  subtract1ToCounter();
});
const subtract1ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract1/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const subtract10Button = document.getElementById('Subtract10Button');
subtract10Button.addEventListener('click', () => {
  subtract10ToCounter();
});
const subtract10ToCounter = async () => {
  try {
    await fetch('http://localhost:8080/subtract10/count', {
      method: 'PUT'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};


const restartCounter = document.getElementById('ResetButton');
restartCounter.addEventListener('click', () => {
  resetCounter();
});
const resetCounter = async () => {
  try {
    await fetch('http://localhost:8080/counter', {
      method: 'DELETE'
    });
    refreshCounters();
  } catch (error) {
    console.log(error);
  }
};

});

addVisitorButton.addEventListener('click', () => {
  const visitorName = visitorNameInput.value.trim();

  if (visitorName !== '') {
    addVisitor(visitorName);
    visitorNameInput.value = '';
  }
});

const addVisitor = async (visitorName) => {
  try {
    await fetch('http://localhost:8080/visitors', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name: visitorName, role: 'visitor' })
    });

    refreshVisitorList();
  } catch (error) {
    console.log(error);
  }
};


ipcRenderer.invoke('getVisitors')
  .then((data) => {
    const visitorNamesElement = document.getElementById('visitors');
    visitorNamesElement.innerHTML = '';
    data.forEach(visitor => {
      const p = document.createElement('p');
      p.innerText = `Name: ${visitor.name}, Role: ${visitor.role}`;
      visitorNamesElement.appendChild(p);
    });
  })
  .catch((err) => {
    console.log(err);
  });

const poblateDataBaseButton = document.getElementById('poblateDataBase');

poblateDataBaseButton.addEventListener('click', async () => {
  try {
    const response = await fetch('http://localhost:8080/populate', {
      method: 'POST'
    });
    console.log("the database has been poblated");
    if (response.ok) { // si la respuesta del servidor es 200 OK
      window.alert('La base de datos ha sido poblada con éxito.');
    } else {
      window.alert('Ocurrió un error al poblar la base de datos.');
    }
 } catch (error) {
    console.log(error);
  }
});


getDocumentButton.addEventListener('click', async () => {
  try {
    const response = await fetch('http://localhost:8080/documents/el_quijote.txt');

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData.error);
    }

    const reader = response.body.getReader();

    let text = '';

    while (true) {
      const { done, value } = await reader.read();

      if (done) {
        documentDataElement.innerText = text;
        break;
      }

      text += new TextDecoder().decode(value);
    }
  } catch (error) {
    console.log(error);
    documentDataElement.innerText = 'An error occurred while fetching the document: ' + error.message;
  }
});

uploadButton.addEventListener('click', () => {
  const file = fileUpload.files[0];
  if (file) {
    const reader = new FileReader();
    reader.onload = function(evt) {
      let partOrder = 0;
      let data = evt.target.result;
      while (data.length > 0) {
        const part = data.slice(0, 500000); // Enviar en partes de 500000 caracteres
        data = data.slice(500000);
        const xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://localhost:8080/documents', true);
        xhr.setRequestHeader('Content-Type', 'application/octet-stream');
        xhr.send(file.name + '\n' + partOrder + '\n' + part);
        xhr.onload = function () {
          if (xhr.status === 200) {
            console.log('Part uploaded successfully');
          } else {
            console.error('Error uploading part');
          }
        };
        partOrder++;
      }
      window.alert('El archivo se ha subido correctamente.');
    };
    reader.readAsText(file);
  }
});




setInterval(refreshCounters, 1000);
setInterval(refreshVisitorList, 5000);