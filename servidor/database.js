var mysql = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'Madrid28033',
    database : 'mysqlDB'
  });

  connection.connect(function(err) {
    if (err) throw err;
    console.log("Conectado a la base de datos!");
  });
  

  function getAllVisitors(callback){
    connection.query('SELECT * FROM mysqlDB.visitors', function (err, results){
        if (err) return callback(err);
        callback(null, results);
    })
  }
  const addVisitor = (visitor, callback) => {
    connection.query('INSERT INTO visitors SET ?', visitor, (err, results) => {
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  };

  const getDocumentByName = (name, callback) => {
    connection.query('SELECT * FROM documents WHERE name = ? ORDER BY part_order', [name], (err, results) => {
      if (err) {
        return callback(err);
      }
      if (results.length === 0) { 
        return callback(new Error('Documento no encontrado, por favor poblar la base de datos.'));
      }
      const document = {
        name: results[0].name,
        data: results.map(result => result.data).join('')
      };
      callback(null, document); 
    });
  };
  
  

  const addDocumentToDb = (document, part_order, callback) => {
    connection.query('INSERT INTO documents SET ?, part_order = ?', [document, part_order], (err, results) => {
      if (err) {
        return callback(err);
      }
      callback(null, results);
    });
  };
  
  
  module.exports = { getAllVisitors, addVisitor, addDocumentToDb, getDocumentByName};
