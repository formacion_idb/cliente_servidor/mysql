const http = require('http');
const fs = require('fs');
const path = require('path');




const hostname = '127.0.0.1';
const port = 8080;
const db = require('./database');

let counter = 0;
let maxCount = 0;

function saveDocumentPart(name, data, partOrder) {
  const document = { name, data };
  db.addDocumentToDb(document, partOrder, (err) => {
    if (err) {
      console.error('Error saving document part:', err);
    }
  });
}

const server = http.createServer(async (req, res) => {

    if(req.url == "/counter" && req.method == "GET") {
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } 
    else if (req.url == "/counter" && req.method == "DELETE") {
      counter = 0;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } 
    else if(req.url=="/add1/count" && req.method == "PUT") {
      counter++;
      if (counter > maxCount){
        maxCount = counter;
      }
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    }
     else if(req.url=="/add10/count" && req.method == "PUT") {
      counter += 10;
      if (counter > maxCount){
        maxCount = counter;
      }
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } 
    else if(req.url=="/subtract1/count" && req.method == "PUT") {
      counter--;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } 
    else if(req.url=="/subtract10/count" && req.method == "PUT") {
      counter -= 10;
      res.statusCode = 200;
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(counter));
    } 
    else if(req.url=="/maxCounter" && req.method == "GET") {
        res.statusCode = 200;
        res.setHeader('Content-type', 'application/json');
        res.end(JSON.stringify(maxCount));
    } 
    else if(req.url == "/visitors" && req.method == "GET") {
      db.getAllVisitors((err, visitors) => {
        if(err) {
          res.statusCode = 500;
          res.setHeader('Content-type', 'application/json');
          res.end(JSON.stringify({error: 'An error occurred'}));
        } else {
          res.statusCode = 200;
          res.setHeader('Content-type', 'application/json');
          res.end(JSON.stringify(visitors));
        }
      });
    }
    else if (req.url == "/visitors" && req.method == "POST") {
      let body = '';
      req.on('data', chunk => body += chunk);
      req.on('end', () => {
        const visitor = JSON.parse(body);
        db.addVisitor(visitor, (err) => {
          if (err) {
            res.statusCode = 500;
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify({error: 'An error occurred'}));
          } else {
            res.statusCode = 200;
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify(visitor));
          }
        });
      });
    }

    else if (req.method === 'POST' && req.url === '/populate') {
      fs.readFile(path.join(__dirname, 'res', 'el_quijote.txt'), 'utf8', (err, data) => {
        if (err) {
          console.log(err);
          res.statusCode = 500;
          res.end();
          return;
        }
        const fileName = 'el_quijote.txt';
        let partOrder = 0;
        while (data.length > 0) {
          const part = data.slice(0, 500000); // Enviar en partes de 10000 caracteres
          data = data.slice(500000);
          saveDocumentPart(fileName, part, partOrder++);
        }
        res.statusCode = 200;
        res.end();
      });
    }
    
      else if (req.method === 'GET' && req.url.startsWith('/documents/')) {
        const documentName = req.url.split('/').pop();
      
        db.getDocumentByName(documentName, (err, document) => {
          if (err) {
            res.statusCode = 500;
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify({error: 'An error occurred'}));
          } else if (!document || !document.data) {
            res.statusCode = 404;
            res.setHeader('Content-type', 'application/json');
            res.end(JSON.stringify({error: 'Documento no encontrado, debe poblar la base de datos.'}));
          } else {
            res.statusCode = 200;
            res.setHeader('Content-type', 'text/plain');
            res.end(document.data);
          }
        });
      }
      else if (req.method === 'POST' && req.url === '/documents') {
        let chunks = [];
        let fileName = '';
        let partOrder = 0;
        req.on('data', (chunk) => {
          const separator = chunk.indexOf('\n'); 
          if (separator !== -1 && !fileName) {
            fileName = chunk.slice(0, separator).toString();
            chunks.push(chunk.slice(separator+1));
          } else {
            chunks.push(chunk);
          }
          if (chunks.join('').length >= 500000) { // Guardar cada 500000 caracteres
            saveDocumentPart(fileName, chunks.join(''), partOrder++);
            chunks = [];
          }
        });
      
        req.on('end', () => {
          if (chunks.length > 0) { // Guardar los caracteres restantes
            saveDocumentPart(fileName, chunks.join(''), partOrder);
          }
          res.statusCode = 200;
          res.setHeader('Content-type', 'application/json');
          res.end(JSON.stringify({ name: fileName }));
        });
      }
      
      
      else {
        res.statusCode = 404;
        res.end("Page not found");
      }
  });



server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});


